Vim for Windows
===============

--------------------------------------------------------------------------------

    :::text
                                          ▒██▓
                                         ██████▓
          ▓███████████████████████████████▒▒▓▒███░  ▓█████████████████████████████
         ███                           ██▓▓▓▓▓▒▒██▓███                          ███
         ██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒███▓▓▓▓█▓▒▒███▒ ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓██░
         ██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░▒▓███▓▓▓▓▓▓█▓▒██▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓██░
         ██▓ ▒▓▓▓ ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓▓▓███▓▓▓▓▓▓▓▓▓██▒ ▒▒▒▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓███░
         ▓██████  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓██████▓▓▓▓▓▓▓▓▓▓███▓███▒▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓████
          ░█████  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓█████▓▓▓▓▓▓▓▓▓▓▓▓▓████▒ ▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓████▒
             ▒██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓██▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓███▒ ▒▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓███▓
             ▒██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓██▓▓▓▓▓▓▓▓▓▓▓▓▓███▓  ▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓████
             ▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓██▓▓▓▓▓▓▓▓▓▓▓████░ ▒▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓████▒
             ▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓██▓▓▓▓▓▓▓▓▓████▒ ▒▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓███▓
             ▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓██▓▓▓▓▓▓▓▓███▓ ░▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓███▓
             ▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓██▓▓▓▓▓▓███▓  ▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓██▓░
             ▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓██▓▓▓▓████▒ ▒▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓█████░
             ▒██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓██▓▓▓███▒ ░▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓████▓▒▓██
             ▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓██████▓  ▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓███▓▓▓▓▒▒██▓
           ░████  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓█████░ ▒▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓███▓▓▓▓▓▓█▓▒▓██▓
          ███▒██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓███▒ ▒▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓████▓▓▓▓▓▓▓▓▓█▓▒▓██░
        ▓██▒░▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓█▓  ▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓████▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒▒███
      ▒██▓▒▓▓███  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓  ▒▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓███▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█▓▒▓██▓
    ░███▒▒▓▓▓▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓░▒▓▒▒▒▒▒▒▒▒▒▒▒▒░▒▒▓▓████▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒▓██▓
    ▓██▓▓█▓▓▓▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓██████▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓██▓███
     ▒███▓█▓▓▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒████▓▓██▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█████▓
       ▓███▓▓███  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░▓█▒ ░ ░██▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█████▓
         ▓██▓▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓█▓ ░░░▓█▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓██████
          ░█████  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓███████▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓████▓░
            ▒███  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓█████████▓▓███████▓▓▓▓██████████▓▒▒▓▓▓
             ▒██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓██▓▓███▓██▓██▓██████████████████████████▒
             ▒██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓██▓▓▒░░░ ████▓▓░░░░░▓██▓▒░░░░▒▓▓▓▓░░▒▒░▒██
             ▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓██████▒▒▒░▓█▓▓██▓▒▒▒▒░░  ░▒▒▒▒▒░░░░░▒▒▒▒░▓█▓
             ▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓████▓▓█▒▒▒▒▒██▓▓██▒▒▒▒██████▒▒▒▒▒█████▓▒▒▒▒██
             ▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓███▓▓▓█▓░▒▒░██▓▓▓█▒░▒░▓██████▒▒▒▒██▒░██▒▒▒░▓█▓
             ▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓████▓▓▓██▒▒▒▒▒██▓▓██░▒▒▒██▓▓██▒▒▒░▓█▒ ░█▓░▒▒▒██
             ▓██  ▒▒▒▒▒▒▒▒▒▒▒▒▒▓████▓▓▓▓▓█▓░▒▒░██▓▓██▒▒▒░▓██▓██▓░▒▒▒██  ██▒▒▒░▓█▒
             ▓██  ▒▒▒▒▒▒▒▒▒▒▒▓▓███▓▓▓▓▓▓██░▒▒░▓█▓▓▓█▓░▒▒▒██████▒▒▒░▓█░ ▒█▓░▒▒▒██
             ▓██  ▒▒▒▒▒▒▒▒▒▓▓████▓▓▓▓▓▓██▒░▒▒▒██████░░▒ ▓██▓░█▓ ▒░▒███ ██░▒▒ ▓███
             ▓██  ▒▒▒▒▒▒▒▒▓██████▓█▓▓▓▓██▒▒▒▒▒▒▓███▓▒▒▒▓▓██ ██▓▓▓▓▓▓█▓▒██▓▓▓▓▓██▓
             ▓██   ▒▒▒▒▒▓████▒ ▓███▓█▓███████████▓████████▓ █████████ ██████████
             ▒█████████████▓     ▓██▓▓▓▓▓▓▓▓▓▓▓▓▓▓█████░
               ▓██████████        ░███▓▓▓▓▓▓▓▓▓▓█████▓
                                    ▓███▓█▓▓▓▓█████▓
                                      ▓███▓███████
                                        ████████▒
                                         ▒████▓
                                          ▒██▓

Table of Contents
-----------------

--------------------------------------------------------------------------------

  - [Description](#markdown-header-description)
  - [Platforms](#markdown-header-platforms)
  - [Architectures](#markdown-header-architectures)
  - [Installation](#markdown-header-installation)
      - [Python](#markdown-header-python)
  - [A Note to Bram](#markdown-header-a-note-to-bram)
  - [Downloads](#markdown-header-downloads)

Description
-----------

--------------------------------------------------------------------------------

Building [Vim (Vi IMproved)][Wikipedia/Vim] with decent set of features for
Windows is quite tricky and frustrating because its build system is just insane
piece of crap.

There are several projects that tried or are trying to provide Vim builds for
Windows. However, some of them are either already dead or struggle with crashes,
for example, because of using miserable toolchains (like
[Visual C++][Wikipedia/Visual C++]) and/or lack of experience in building
software in general. I'm not going to poke a finger in these projects or provide
any links to them as this is irrelevant for further discussion after all.

The goal of this project is to provide high-quality native builds of Vim for
Windows for both x86 (32-bit) and x64 (64-bit) architectures with "huge" feature
set and support for Python 2, Python 3, and Ruby simultaneously. The latest
source code used for builds is obtained directly from
[official Vim repository][Vim/Repository] maintained by its author,
[Bram Moolenaar][Wikipedia/Bram Moolenaar].

**NOTE:** If you are interested which toolchain I use to build Vim for Windows
as well as other native software for Windows in general, then I'd be glad to say
that I'm using [MinGW-w64][] (not [MinGW][]!) which is a production quality
toolchain, bringing bleeding-edge [GCC][Wikipedia/GCC] features to Windows for
both x86 and x64 architectures.

Platforms
---------

--------------------------------------------------------------------------------

  - Windows 2000;
  - Windows XP;
  - Windows Vista;
  - Windows 7;
  - Windows 8.

Architectures
-------------

--------------------------------------------------------------------------------

  - x86, x86-32, x32, i686;
  - x64, x86-64, amd64.

Installation
------------

--------------------------------------------------------------------------------

 1. Go to [Downloads](#markdown-header-downloads) and obtain the archive with
    desired version for your architecture;
 2. Extract the archive wherever you like;
 3. Enjoy, and happy Vimming! `:)`

### Python

I'm going to talk about trivial stuff here, but last time some people had issues
even with this part. Can you believe that?! `o_O`

For [Python][Wikipedia/Python] to work properly, all you have to do is to make
Python DLLs (with versions that your chosen Vim distribution expects) visible to
both `vim.exe` and `gvim.exe`. You have 2 options to do that:

 1. Drop Python DLLs into extracted Vim distribution, i.e. on the same level as
    `vim.exe` and `gvim.exe`;
 2. Add paths to directories containing Python DLLs to the `PATH` environment
    variable.

Of course in overwhelming majority of cases you should choose option \#2 because
that's how normal people customize their environment, right? But if you are
still at school and don't know what `PATH` is, then I guess option \#1 is fine,
keep trying tho.

The next important thing to understand is that the targeted architecture of
Python DLLs has to match the targeted architecture of the Vim distribution you
chosen. In other words, if you have chosen 64-bit Vim distribution, then you
should expose 64-bit Python DLLs to it. Got it? Good.

Python uses natural and widely accepted versioning scheme:

    :::text
    <version> = <major>.<minor>.<patch>

Changes in the interface can only occur when `<major>` and/or `<minor>` numbers
are incremented, i.e. increments in `<patch>` number do not break compatibility.
What that means for you is that you should not care about what was the `<patch>`
number of Python when these Vim distributions were built. For example, if I
linked Vim distribution that you've chosen against Python 2.7.5, but you are
still sitting on Python 2.7.3, then it is not necessary for you to update to
Python 2.7.5. On the other hand, if you downloaded Vim distribution which was
linked against, say, Python 3.3.2, but you have Python 3.2.3, then it is
absolutely necessary for you to update to Python 3.3.x. If you understood the
concept correctly, then you should understand why there is x, rather than 2.
Finally, if you recall how Python DLLs are named, i.e.
`python<major><minor>.dll`, e.g. `python33.dll`, then you'd see that all the
aforementioned suddenly makes sense again.

Although `<major>` and `<minor>` numbers of required Python versions are
listed in the [Downloads](#markdown-header-downloads) and appear in names of
archives, you can still find out which versions are required by your Vim
distribution from either `vim.exe` or `gvim.exe` directly by simply typing
`:version`. Look to the bottom of the received message and you should see
something similar to:

    :::text
    Dependency: python27.dll, python33.dll

Now this tells you exactly which versions of Python DLLs your Vim distribution
expects. Pay attention to the address model as well, for example

    :::text
    MS-Windows 64-bit GUI version with OLE support

tells you that your Vim distribution is targeted at x64 architecture, and you
have to expose Python DLLs with the same x64 architecture accordingly.

Last but not least, not to mess things up, I highly recommend that you simply
download [official Windows MSI installers][Python/Downloads] which are relevant
to your Vim distribution (according to above instructions).

**NOTE:** These Windows MSI installers will put Python DLLs into system
directories

  - `%SystemRoot%\System32`;
  - `%SystemRoot%\SysWOW64`.

which are included into the `PATH` environment variable by default, and
therefore you don't have to do any additional manipulations to expose Python
DLLs to your Vim distribution because everything should already work
out-of-the-box.

To test whether Python 2 works properly with your Vim distribution, type:

    :::text
    :py import sys; print(sys.version)

To test whether Python 3 works properly with your Vim distribution, type:

    :::text
    :py3 import sys; print(sys.version)

A Note to Bram
--------------

--------------------------------------------------------------------------------

Dear Mr. Moolenaar,

Do you really think that Vim as a huge piece of software aiming to be
cross-platform with its large code base benefits from manually written makefiles
and shell scripts as its current build system?

I hope it is apparent that the answer is: _"No"_. My personal recommendation
would be to consider migration to [CMake][Wikipedia/CMake]. It fits the
philosophy of large-scale cross-platform projects like Vim very well.

To be honest, I currently find Vim to be decaying. As a project it is a good
example of how modern projects should never be organized. Here are a few points:

  - Directory structure is inadequate;
  - Names of some files and directories are inadequate;
  - Hand-written makefiles are bug-prone (and they, in fact, do contain bugs
    which I have to resolve before providing these builds);
  - Shell scripts, which are there to assist makefiles (since
    [Make][Wikipedia/Make] is very limited), are bug-prone too and do not
    represent cross-platform solution;
  - The notion of out-of-source builds simply does not exist in the world of Vim
    and Mr. Moolenaar, what results in a complete mess inside the source tree
    aka _"Welcome to 90s son!"_;
  - Some parts of Vim source code are written sloppily and require redesign.

Kind regards,  
Alexander Shukaev

Downloads
---------

--------------------------------------------------------------------------------

**NOTE:** Links are in the following format:

    :::text
    Vim                     Python 2        Python 3         Ruby
    ----------------------- --------------- ---------------  -----------------------
    <major>.<minor>.<patch>/<major>.<minor>/<major>.<minor>[/<major>.<minor>.<patch>]

  - x86:
      - **[7.4.094/2.7/3.3/2.0.0][Downloads/Vim/7.4.094/Python/2.7/Python/3.3/Ruby/2.0.0/Windows/x86]**;
      - [7.4.020/2.7/3.3][Downloads/Vim/7.4.020/Python/2.7/Python/3.3/Windows/x86];
      - [7.4a.044/2.7/3.3][Downloads/Vim/7.4a.044/Python/2.7/Python/3.3/Windows/x86].

  - x64:
      - **[7.4.094/2.7/3.3/2.0.0][Downloads/Vim/7.4.094/Python/2.7/Python/3.3/Ruby/2.0.0/Windows/x64]**;
      - [7.4.020/2.7/3.3][Downloads/Vim/7.4.020/Python/2.7/Python/3.3/Windows/x64];
      - [7.4a.044/2.7/3.3][Downloads/Vim/7.4a.044/Python/2.7/Python/3.3/Windows/x64].

[Wikipedia/Bram Moolenaar]:   http://en.wikipedia.org/wiki/Bram_Moolenaar
[Wikipedia/CMake]:            http://en.wikipedia.org/wiki/CMake
[Wikipedia/GCC]:              http://en.wikipedia.org/wiki/GNU_Compiler_Collection
[Wikipedia/Make]:             http://en.wikipedia.org/wiki/Make_(software)
[Wikipedia/Python]:           http://en.wikipedia.org/wiki/Python_(programming_language)
[Wikipedia/Vim]:              http://en.wikipedia.org/wiki/Vim_(text_editor)
[Wikipedia/Visual C++]:       http://en.wikipedia.org/wiki/Visual_C%2B%2B

[Vim/Repository]: http://code.google.com/p/vim/

[Python/Downloads]: http://www.python.org/download/

[MinGW]:     http://www.mingw.org/
[MinGW-w64]: http://mingw-w64.sourceforge.net/

[Downloads/Vim/7.4.094/Python/2.7/Python/3.3/Ruby/2.0.0/Windows/x86]: /Haroogan/vim-for-windows/downloads/vim-7.4.094-python-2.7-python-3.3-ruby-2.0.0-windows-x86.zip
[Downloads/Vim/7.4.094/Python/2.7/Python/3.3/Ruby/2.0.0/Windows/x64]: /Haroogan/vim-for-windows/downloads/vim-7.4.094-python-2.7-python-3.3-ruby-2.0.0-windows-x64.zip

[Downloads/Vim/7.4.020/Python/2.7/Python/3.3/Windows/x86]: /Haroogan/vim-for-windows/downloads/vim-7.4.020-python-2.7-python-3.3-windows-x86.zip
[Downloads/Vim/7.4.020/Python/2.7/Python/3.3/Windows/x64]: /Haroogan/vim-for-windows/downloads/vim-7.4.020-python-2.7-python-3.3-windows-x64.zip

[Downloads/Vim/7.4a.044/Python/2.7/Python/3.3/Windows/x86]: /Haroogan/vim-for-windows/downloads/vim-7.4a.044-python-2.7-python-3.3-windows-x86.zip
[Downloads/Vim/7.4a.044/Python/2.7/Python/3.3/Windows/x64]: /Haroogan/vim-for-windows/downloads/vim-7.4a.044-python-2.7-python-3.3-windows-x64.zip
